export class Block {
    #blockTypes = 'ILJOTSZ';
    coordinates = {x: 5, y: 5};

    colors = [
        null,
        '#FF0D72',
        '#0DC2FF',
        '#0DFF72',
        '#F538FF',
        '#FF8E0D',
        '#FFE138',
        '#3877FF',
    ];

    shape = [
        [5, 5, 0],
        [0, 5, 5],
        [0, 0, 0],
    ];

    constructor() {

    }

    collide(arena) {
        const [playerMatrix, playerPosition] = [this.shape, this.coordinates];
        for(let y = 0; y < playerMatrix.length; ++y) {
            for(let x = 0; x < playerMatrix[y].length; ++x) {
                if(playerMatrix[y][x] !== 0
                    && (arena[y+playerPosition.y] && arena[y + playerPosition.y][x + playerPosition.x]) !== 0) {
                    return true;
                }
            }
        }
        return false;
    }

    setShapeByType(type) {
        if (type === 'I') {
            this.shape = [
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 0, 0],
            ];
        } else if (type === 'L') {
            this.shape = [
                [0, 2, 0],
                [0, 2, 0],
                [0, 2, 2],
            ];
        } else if (type === 'J') {
            this.shape = [
                [0, 3, 0],
                [0, 3, 0],
                [3, 3, 0],
            ];
        } else if (type === 'O') {
            this.shape = [
                [4, 4],
                [4, 4],
            ];
        } else if (type === 'Z') {
            this.shape = [
                [5, 5, 0],
                [0, 5, 5],
                [0, 0, 0],
            ];
        } else if (type === 'S') {
            this.shape = [
                [0, 6, 6],
                [6, 6, 0],
                [0, 0, 0],
            ];
        } else if (type === 'T') {
            this.shape = [
                [0, 7, 0],
                [7, 7, 7],
                [0, 0, 0],
            ];
        }
    }

    createRandomBlock() {
        this.setShapeByType(this.#blockTypes[this.#blockTypes.length * Math.random() | 0]);
    }

    moveUp() {
        this.coordinates.y-=1;
    }

    moveDown() {
        this.coordinates.y+=1;
    }

    moveLeft() {
        this.coordinates.x-=1;
    }

    moveRight() {
        this.coordinates.x+=1;
    }

    rotate(matrix, direction) {
        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < y; ++x) {
                [
                    matrix[x][y],
                    matrix[y][x],
                ] = [
                    matrix[y][x],
                    matrix[x][y],
                ];
            }
        }

        if(direction > 0) {
            matrix.forEach(row => row.reverse());
        } else {
            matrix.reverse();
        }
    }

    rotateBlock(direction, tetrisArena) {
        const positionX = this.coordinates.x;
        let offset = 1;
        this.rotate(this.shape, direction);
        while (this.collide(tetrisArena)) {
            this.coordinates.x += offset;
            offset = -(offset + (offset > 0 ? 1 : -1));
            if (offset > this.shape[0].length) {
                this.rotate(this.shape, -direction);
                this.coordinates.x = positionX;
                return;
            }
        }
    }
}
